#-------------------------------------------------
#
# Project created by QtCreator 2015-04-06T19:25:55
#
#-------------------------------------------------

QT +=       core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET =    LightList
TEMPLATE =  app

@
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
@

SOURCES +=  main.cpp\
            MainWindow.cpp

HEADERS +=  MainWindow.h

FORMS +=    MainWindow.ui

include(QDownloadManager/QDownloadManager.pri)

#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mDownloadManager = new QDownloadManager(this);
    connect(mDownloadManager, SIGNAL(downloadFinished(QString)), this, SLOT(on_downloadFinished(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_downloadFinished(QString name)
{
    qDebug() << "on_downloadFinished() received: " << name;
    if(!mDownloadManager->dataAvailable(name))
        return;
}

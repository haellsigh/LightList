var options = {
    keepHistory: 1000 * 60 * 5,
    localSearch: true
};
var fields = ['title', 'episodes', 'score'];

AnimeSearch = new SearchSource('animes', fields, options);

Template.animeList.helpers({
    getAnimes: function() {
        return AnimeSearch.getData();
    },

    isLoading: function() {
        return AnimeSearch.getStatus().loading;
    }
});

Template.animeList.events({
    'click .removeAnime': function(e) {
        Meteor.call("removeAnime", this._id, function(error, result) {
            console.log("removed anime: " + result);
            Session.set("")
        })
        //Animes.remove(this._id);
    },
    'keyup #searchBox': _.throttle(function(e) {
        var text = e.target.value.trim();
        AnimeSearch.search(text);
    }, 200),
    'click .addDummyAnime': function(e) {
        var anime = {
            "episodes":42,
            "title":"Dummy Animu",
            "synopsis":"Dummy Synopsis.",
            "cover":"http://cdn.myanimelist.net/images/anime/5/71553.jpg",
            "status":"Finished airing",
            "genres":["Action","Adventure","Comedy"]
        };

        console.log("Adding dummy anime.");

        Meteor.call("insertAnime", anime);
    }
});

Template.animeList.rendered = function() {
    AnimeSearch.search('');
}

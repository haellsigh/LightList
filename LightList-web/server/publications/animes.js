Meteor.publish("animes.search", function() {
    console.log("Requesting from: " + this.userId);

    return Animes.find({}, {fields: {
        'title': 1,
        'episodes': 1,
        'score': 1
    }});
});

Meteor.publish("animes", function() {
    return Animes.find();
});

Meteor.publish("animes.one", function(_id) {
    return Animes.find({_id: _id});
});

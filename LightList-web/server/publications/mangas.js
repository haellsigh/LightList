Meteor.publish("mangas.search", function() {
    return Mangas.find({}, {fields: {
        'title': 1,
        'episodes': 1,
        'score': 1
    }});
});

Meteor.publish("mangas", function() {
    return Mangas.find();
});

Meteor.publish("mangas.one", function(_id) {
    return Mangas.find({_id: _id});
});

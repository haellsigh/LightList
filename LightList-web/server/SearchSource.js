SearchSource.defineSource('animes', function(searchText, options) {
    if(searchText) {
        var regExp = buildRegExp(searchText);
        console.log("RegExp:" + regExp);
        console.log(Animes.find({$or: [{title: regExp}, {summary: regExp}, {episodes: regExp}, {score: regExp}]}, {fields: {
            'title': 1,
            'episodes': 1,
            'score': 1
        }}).fetch());
        return Animes.find({$or: [{title: regExp}, {episodes: regExp}, {score: regExp}]}, {fields: {
            'title': 1,
            'episodes': 1,
            'score': 1
        }}).fetch();
    } else {
        return Animes.find({}, {fields: {
            'title': 1,
            'episodes': 1,
            'score': 1
        }}).fetch();
    }
});

function buildRegExp(searchText) {
    return new RegExp(".*" + searchText + ".*", "i");
}

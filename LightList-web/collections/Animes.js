Animes = new Mongo.Collection('animes');

var SchemasAnimes = {};

// 10 5 6 8 8

SchemasAnimes.Score = new SimpleSchema({
    score: {
        type: Number,
        label: "Score"
    },
    user: {
        type: String,
        label: "User"
    }
})

SchemasAnimes.SimpleType = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },
    type: {
        type: String,
        label: "Type",
        allowedValues: ['Anime', 'Manga']
    },
    _id: {
        type: String,
        label: "Id"
    }
})

SchemasAnimes.Anime = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },
    synopsis: {
        type: String,
        label: "Synopsis"
    },
    cover: {
        type: String,
        label: "Cover url",
        max: 400,
        regEx: SimpleSchema.RegEx.Url
    },
    episodes: {
        type: Number,
        label: "Episodes",
        min: 0
    },
    status: {
        type: String,
        label: "Status",
        allowedValues: ['Currently airing', 'Finished airing', 'Not yet aired']
    },
    genres: {
        type: [String],
        label: "Genres",
        allowedValues: ['Action','Adventure','Cars','Comedy','Dementia','Demons','Drama','Ecchi','Fantasy','Game','Harem','Hentai','Historical','Horror','Josei','Kids','Magic','Martial Arts','Mecha','Military','Music','Mystery','Parody','Police','Psychological','Romance','Samurai','School','Sci-Fi','Seinen','Shoujo','Shoujo Ai','Shounen','Shounen Ai','Slice of Life','Space','Sports','Super Power','Supernatural','Thriller','Vampire','Yaoi','Yuri']
    },
    relations: {
        type: [Object],
        label: "Relations",
        optional: true
    },
    "relations.$.relation": {
        type: String,
        label: "Relation",
        allowedValues: ['Prequel', 'Sequel', 'Adaptation']
    },
    "relations.$.anime": {
        label: "Anime",
        type: SchemasAnimes.SimpleType
    }
    /*

     */

});
Animes.attachSchema(SchemasAnimes.Anime);

Animes.allow({
    insert: function() {
        //TODO Check if current user has enough privilege to add an Anime
        return true;
    },
    remove: function() {
        //TODO Check if current user has enough privilege to remove an Anime
        return true;
    }
})

Meteor.methods({
    "insertAnime": function(doc) {
        //IMPL Check if current user has enough privilege to add an anime
        return Animes.insert(doc);
    },
    "removeAnime": function(doc) {
        //IMPL Check if current user has enough privilege to remove an anime
        return Animes.remove(doc);
    }
});

Mangas = new Mongo.Collection("mangas");

var SchemasManga = {};

// 10 5 6 8 8

SchemasManga.Score = new SimpleSchema({
    score: {
        type: Number,
        label: "Score"
    },
    user: {
        type: String,
        label: "User"
    }
})

SchemasManga.SimpleType = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },
    type: {
        type: String,
        label: "Type",
        allowedValues: ['Anime', 'Manga']
    },
    _id: {
        type: String,
        label: "Id"
    }
})

SchemasManga.Manga = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 200
    },
    synopsis: {
        type: String,
        label: "Synopsis"
    },
    cover: {
        type: String,
        label: "Cover url",
        max: 400,
        regEx: SimpleSchema.RegEx.Url
    },
    chapters: {
        type: Number,
        label: "Chapters",
        min: 0
    },
    status: {
        type: String,
        label: "Status",
        allowedValues: ['Publishing', 'Finished', 'Not yet published']
    },
    genres: {
        type: [String],
        label: "Genres",
        allowedValues: ['Action','Adventure','Cars','Comedy','Dementia','Demons','Drama','Ecchi','Fantasy','Game','Harem','Hentai','Historical','Horror','Josei','Kids','Magic','Martial Arts','Mecha','Military','Music','Mystery','Parody','Police','Psychological','Romance','Samurai','School','Sci-Fi','Seinen','Shoujo','Shoujo Ai','Shounen','Shounen Ai','Slice of Life','Space','Sports','Super Power','Supernatural','Thriller','Vampire','Yaoi','Yuri']
    },
    relations: {
        type: [Object],
        label: "Relations",
        optional: true
    },
    "relations.$.relation": {
        type: String,
        label: "Relation",
        allowedValues: ['Prequel', 'Sequel', 'Adaptation']
    },
    "relations.$.anime": {
        label: "Anime",
        type: SchemasManga.SimpleType
    }
    /*

     */

});
Mangas.attachSchema(SchemasManga.Manga);

Mangas.allow({
    insert: function() {
        //TODO Check if current user has enough privilege to add a Manga
        return true;
    }
})

Meteor.methods({
    "insertManga": function(doc) {
        //IMPL Check if current user has enough privilege to add an anime
        return Mangas.insert(doc);
    }
});

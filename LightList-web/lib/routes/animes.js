Router.route('/anime/list', {
    name: 'anime.list',
    data: function() {
        var animes = Animes.find();
        return {
            animes: animes
        };
    },
    waitOn: function() {
        return Meteor.subscribe("animes.search");
    }
});

Router.route('/anime/top', {
    name: 'anime.top',
    data: function() {
        var animes = Animes.find();
        return {
            animes: animes
        };
    },
    waitOn: function() {
        return Meteor.subscribe("animes.search");
    }
});

Router.route('/anime/create', {
    name: 'anime.create',
    waitOn: function() {
        return Meteor.subscribe("animes");
    }
});

Router.route('/anime/edit/:_id', {
    name: 'anime.edit',
    data: function() {
        var anime = Animes.findOne();

        return anime;
    },
    waitOn: function() {
        return Meteor.subscribe("animes.one", this.params._id);
    }
});

Router.route('/anime/:_id', {
    name: 'anime.detail',
    data: function() {
        var anime = Animes.findOne();

        return anime;
    },
    waitOn: function() {
        return Meteor.subscribe("animes.one", this.params._id);
    }
});

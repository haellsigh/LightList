Router.route('/manga/list', {
    name: 'manga.list',
    data: function() {
        var mangas = Mangas.find({});
        return {
            mangas: mangas
        };
    },
    waitOn: function() {
        return Meteor.subscribe("mangas.search");
    }
});

Router.route('/manga/top', {
    name: 'manga.top',
    data: function() {
        var mangas = Mangas.find();
        return {
            mangas: mangas
        };
    },
    waitOn: function() {
        return Meteor.subscribe("mangas.search");
    }
});

Router.route('/manga/create', {
    name: 'manga.create',
    waitOn: function() {
        return Meteor.subscribe("mangas");
    }
});

Router.route('/manga/edit/:_id', {
    name: 'manga.edit',
    data: function() {
        var manga = Mangas.findOne();

        return manga;
    },
    waitOn: function() {
        return Meteor.subscribe("mangas.one", this.params._id);
    }
});

Router.route('/manga/:_id', {
    name: 'manga.detail',
    data: function() {
        var manga = Mangas.findOne();

        return manga;
    },
    waitOn: function() {
        return Meteor.subscribe("mangas.one", this.params._id);
    }
});

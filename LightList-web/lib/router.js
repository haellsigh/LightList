Router.configure({
    layoutTemplate: 'mainLayout',
    progressSpinner: false
})

Router.plugin('dataNotFound', {notFoundTemplate: 'notFound'});

Router.route('/', {
    name: 'landing'
})
